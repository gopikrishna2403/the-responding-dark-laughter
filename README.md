# The Responding Dark Laughter (TRDL)

TRDL is a jetty Server based web service built using Java and deployed using Gitlab CI pipeline and AWS services.
 
The main aim of this application is to use it for a proof of concept (POC) for Gitlab CI pipeline using AWS cloud services.

Prerequisites:
--------------
1. Gitlab account
2. AWS account
3. Docker

Setup:
--------------
This section explains setting up of different services needed for running the pipeline available in this repo.

1. AWS IAM Users
2. ECR
3. AWS CodeArtifact 
4. EKS Cluster using Terraform
5. Gitlab CI Setup

For you perform the steps below with AWS services, it is mandatory to have a user which can manage AWS services such as ECR, IAM, CodeArtifact and EKS.

AWS Region: us-east-2 (Ohio) 

### AWS IAM USERS ###

It is mandatory to have a user who can create/manage IAM users and policies

#### Setup IAM policies for gitlab CI/CD user ####

Policy to manage CodeArtifact:

policy name: TheRespondingDarkLaughterCIPushCodeArtifact

policy in json:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "codeartifact:DisassociateExternalConnection",
                "codeartifact:AssociateWithDownstreamRepository",
                "codeartifact:GetPackageVersionReadme",
                "codeartifact:PutRepositoryPermissionsPolicy",
                "codeartifact:DeletePackageVersions",
                "codeartifact:DescribePackageVersion",
                "codeartifact:GetDomainPermissionsPolicy",
                "codeartifact:DisposePackageVersions",
                "codeartifact:GetAuthorizationToken",
                "codeartifact:ReadFromRepository",
                "codeartifact:GetPackageVersionAsset",
                "sts:GetServiceBearerToken",
                "codeartifact:CreateDomain",
                "codeartifact:DescribeRepository",
                "codeartifact:DescribeDomain",
                "codeartifact:AssociateExternalConnection",
                "codeartifact:UpdateRepository",
                "codeartifact:DeleteDomain",
                "codeartifact:CopyPackageVersions",
                "codeartifact:PutPackageMetadata",
                "codeartifact:DeleteRepository",
                "codeartifact:UpdatePackageVersionsStatus",
                "codeartifact:GetRepositoryEndpoint",
                "codeartifact:CreateRepository",
                "codeartifact:PublishPackageVersion",
                "codeartifact:GetRepositoryPermissionsPolicy",
                "codeartifact:PutDomainPermissionsPolicy"
            ],
            "Resource": "*"
        }
    ]
}
```

Policy to manage ECR:

policy name: TheRespondingDarkLaughterCIPushECR

policy in json:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ecr:PutLifecyclePolicy",
                "ecr:PutImageTagMutability",
                "ecr:DescribeImageScanFindings",
                "ecr:StartImageScan",
                "ecr:GetLifecyclePolicyPreview",
                "ecr:CreateRepository",
                "ecr:GetDownloadUrlForLayer",
                "ecr:PutImageScanningConfiguration",
                "ecr:GetAuthorizationToken",
                "ecr:ListTagsForResource",
                "ecr:UploadLayerPart",
                "ecr:PutImage",
                "ecr:UntagResource",
                "ecr:BatchGetImage",
                "ecr:CompleteLayerUpload",
                "ecr:DescribeImages",
                "ecr:TagResource",
                "ecr:StartLifecyclePolicyPreview",
                "ecr:InitiateLayerUpload",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetRepositoryPolicy",
                "ecr:GetLifecyclePolicy"
            ],
            "Resource": "*"
        }
    ]
}
```


#### Setup IAM User for gitlab CI/CD ####

username: the-responding-dark-laughter-ci

Attach the custom policies created in previous step

Note: Make sure to copy access key and Secret key and store it in secure place, it will used in the step where we setup Gitlab CI

Setup IAM User for managing AWS EKS cluster

username: EKS-admin

Attach pre-defined policies:

1. AmazonEKSClusterPolicy
2. AdministratorAccess

Note: Make sure to copy access key and Secret key and store it in secure place, it will used in the step where we setup EKS cluster

### Setup ECR ###

Create a repository with following name : the-responding-dark-laughter

Setup following permissions using the json for the created ECR repo:

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "AllowPushPull",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::796111291282:user/the-responding-dark-laughter-ci"
      },
      "Action": [
        "ecr:CompleteLayerUpload",
        "ecr:CreateRepository",
        "ecr:InitiateLayerUpload",
        "ecr:PutImage",
        "ecr:PutImageScanningConfiguration",
        "ecr:PutImageTagMutability",
        "ecr:PutLifecyclePolicy",
        "ecr:StartImageScan",
        "ecr:StartLifecyclePolicyPreview",
        "ecr:UploadLayerPart"
      ]
    }
  ]
}
```

### Setup AWS Code Artifact ###

Create a repository with following name: maven-central-sinch-repo

Setup following permissions using the json for the created CodeArtifact repo:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::796111291282:user/the-responding-dark-laughter-ci"
            },
            "Action": [
                "codeartifact:AssociateExternalConnection",
                "codeartifact:CopyPackageVersions",
                "codeartifact:DeletePackageVersions",
                "codeartifact:DeleteRepository",
                "codeartifact:DeleteRepositoryPermissionsPolicy",
                "codeartifact:DescribePackageVersion",
                "codeartifact:DescribeRepository",
                "codeartifact:DisassociateExternalConnection",
                "codeartifact:DisposePackageVersions",
                "codeartifact:GetPackageVersionReadme",
                "codeartifact:GetRepositoryEndpoint",
                "codeartifact:ListPackageVersionAssets",
                "codeartifact:ListPackageVersionDependencies",
                "codeartifact:ListPackageVersions",
                "codeartifact:ListPackages",
                "codeartifact:PublishPackageVersion",
                "codeartifact:PutPackageMetadata",
                "codeartifact:PutRepositoryPermissionsPolicy",
                "codeartifact:ReadFromRepository",
                "codeartifact:UpdatePackageVersionsStatus",
                "codeartifact:UpdateRepository"
            ],
            "Resource": "arn:aws:codeartifact:us-east-1:796111291282:repository/sinch/maven-central-sinch-repo"
        }
    ]
}
```

For connecting to this repo, click on "View Connection instructions" in AWS console for CodeArtifact/Repositories/Repository(maven-central-sinch-repo)

[Screenshot 1](https://imgur.com/qt69l0I "Title is optional")

[Screenshot 2](https://imgur.com/bkfwy7q "Title is optional")


### Setup EKS Cluster using AWS cli ###

We create two clusters for Test and Production. So repeat this step twice

Note: Remember to copy cluster name for each cluster created

To be able to perform this step, it is required to have docker installed in your system and a terminal with AWSCLI

To be able get AWSCLI, lets use spin up a docker container with AWSCLI

You can get the Amazon CLI on [Docker-Hub](https://hub.docker.com/r/amazon/aws-cli) <br/>
We'll need the Amazon CLI to gather information so we can build our Terraform file.

```
# Run Amazon CLI
docker run -it --entrypoint /bin/sh amazon/aws-cli:2.0.43

# some handy tools :)
yum install -y jq gzip nano tar git unzip wget

```

Login using the user created for managing EKS cluster: EKS-Admin

```
# Run aws configure command
# And paste Access key and Secret Access key from step where we created EKS-admin user 
aws configure

AWS Access Key ID : **************
AWS Secret Access Key : *************
region name: us-east-2
output format: json
```

Install terraform

```
# Get Terraform

curl -o /tmp/terraform.zip -LO https://releases.hashicorp.com/terraform/0.13.1/terraform_0.13.1_linux_amd64.zip
unzip /tmp/terraform.zip
chmod +x terraform && mv terraform /usr/local/bin/
terraform
```

Clone Repository which contains terraform modules to install EKS cluster from Hashicorp

```
git clone https://github.com/hashicorp/learn-terraform-provision-eks-cluster.git
cd learn-terraform-provision-eks-cluster
```

Run the Terraform modules to install EKS cluster

```
# Initalize your Terraform workspace
terraform init

# Check what is deployed and how many resources are created 
terraform plan

# Finally, apply the modules
terraform apply

```

Install kubectl

```
# Download Release
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"

# Make the kubectl binary executable.
chmod +x ./kubectl

# Move the binary in to your PATH.
mv ./kubectl /usr/local/bin/kubectl

# Test to ensure the version you installed is up-to-date:
kubectl version --client
```

Configure context for accessing cluster

```
#Fetch Cluster name
terraform output

# The following command will get the access credentials for your cluster and automatically configure kubectl
aws eks --region us-east-2 update-kubeconfig --name <Cluster-name>

```

### Setup Gitlab CI Setup  ###

Gitlab CI needs to be setup with EKS and AWS servcies (ECR and CodeArtifact)

#### Create environments  ####

Login to your Gitlab account and go to "the-responding-dark-laughter"

Go to Operations -> Environments

Click on "New Environment"

Provide following input

Name : Test

External URL: <Leave it blank>

Repeat process to create another Environment "Production"

#### Integrate with EKS Cluster  ####

In this step we integrate two clusters, one for Test environment and another for Production environment

##### Test Environment #####

Login to your Gitlab account and go to "the-responding-dark-laughter"

Go to Operations -> Kubernetes

Click on "Connect cluster with certificate"

Click on "Connect existing cluster"

Provide the following input:

Use one cluster name created in step where we spined up EKS cluster using Terraform
Kubernetes Cluster Name: <Paste-cluster-name>

Environment Scope: Test

Following steps as per the suggested links
API URL: 

Following steps as per the suggested links
CA Certificate:

Following steps as per the suggested links
Service Token:

Leave the remaining settings as it is

##### Production Environment #####

Login to your Gitlab account and go to "the-responding-dark-laughter"

Go to Operations -> Kubernetes

Click on "Connect cluster with certificate"

Click on "Connect existing cluster"

Provide the following input:

Use one cluster name created in step where we spined up EKS cluster using Terraform
Kubernetes Cluster Name: <Paste-cluster-name>

Environment Scope: Production

Following steps as per the suggested links
API URL: 

Following steps as per the suggested links
CA Certificate:

Following steps as per the suggested links
Service Token:

Leave the remaining settings as it is

#### Install Ingress Controller for EKS Cluster using Gitlab  ####

Login to your Gitlab account and go to "the-responding-dark-laughter"

Go to Operations -> Kubernetes

Click on one cluster

Go to "Applications" tab

Click on Install for Ingress 

It outputs a URL and make sure to COPY it

Repeat the same procedure for other cluster

#### Create variables in Gitlab CI  ####

Login to your Gitlab account and go to project "the-responding-dark-laughter"

Go to Settings -> CI/CD -> Variables

Create following variables:

AWS_ACCESS_KEY_ID: <Paste-from-step-you-created-gitlab-iam-user>

AWS_SECRET_ACCESS_KEY: <Paste-from-step-you-created-gitlab-iam-user>

Login AWS console, go to ECR and copy ECR repo uri for "the-responding-dark-laughter" 

ECR_REPO: <Paste-ECR-Repo-URI>

As stated in step setting up AWS CodeArtifact (Refer screen shot 2), use "view connections instrctions" button to fetch Code Artifact Repo URL

MAVEN_REPO_URL: <Paste-codeartifact-repo-url>

This pipeline does automatic pushes, so you need to create Personal Access Token. Refer document: [PERSONAL ACCESS TOKEN](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token)

GITLAB_CICD_TOKEN: <Paste-Personal-access-token>

This pipeline does E2E testing in Test Environment so it needs to access a EKS cluster ingress backend.

Copy Ingress URL for Test and Production Environment 

APP_URL: <Paste-Ingress-Controller-URL-For-Test>

APP_URL_PROD: <Paste-Ingress-Controller-URL-For-Production>


Pipeline:
--------------

This section describes different stages in pipeline, how to trigger a pipeline and production deployment

### Pipeline description  ###

There are 10 stages and 12 jobs in this whole CI/CD setup. 

#### Stage 1: Pass Auth Token  ####

This stage is used to fetch Auth token for interacting with AWS CodeArtifact. 
It exports Auth token into a environment variable, which is further used in jobs from other stages

#### Stage 2: Build  ####

This stage is used to compile project.
Uses maven commands to compile project

#### Stage 3: Unit Test  ####

This stage is used to run Unit testing using Junit

Unit tests are triggered using maven commands

Number of Unit Test cases: 7

#### Stage 4: Publish Snapshot Artifacts ####

In this stage we upload snapshot version of project artifacts built using maven commands into AWS CodeArtifact

#### Stage 5: Docker Build ####

This stage is used to build docker image and save in /data directory for next stage to use. Project artifacts used in Dockerfile are snapshot version

Dockerfile used is "Dockerfile"

#### Stage 6: Docker Push With Snapshot Tag ####

This stage uses docker image built in previous step

Docker image is tagged with CI_COMMIT_SHORT_SHA and "-snapshot" suffix

Later newly tagged image is pushed to AWS ECR 

#### Stage 7: Deploy in Test Env ####

This step uses kubernetes mainfest files available in "installation/" directory and deploys it in Test Environment Kubernetes cluster

It applies to only to merge requests towards "master" branch

#### Stage 8: Integration Test ####

In this step we run a http python client which accesses TRDL application to verify its functionality.

Python client used is "integration_test.py"

Number of Test cases: 3

It applies to only to merge requests towards "master" branch

#### Stage 9: Release Artifacts ####

In this step we release artifacts with sharp version using maven commands and deploy it in AWS CodeArtifact

It applies to only to merge requests towards "master" branch

It exports sharp version of app to environment variable "VERSION"

#### Stage 10: Docker Push Release ####

This stage has two jobs. Both the jobs are run in parallel. One job is used to push docker images to AWS ECR and another one is used to step snapshot version

1. Release-Image
2. Step-To-Next-Snapshot

In Release-Image job, it packages sharp version artifacts into docker image built in previous step. 

Later we tag the image with sharp version exported in previous build and push it to AWS ECR

In Step-To-Next-Snapshot job, we step to new version of snapshot, push the changes to master using Personal access token. 

The commit message includes "ci skip" message, which notifies gitlab ci not to trigger pipeline 

#### Stage 11: Deploy in Prod Env ####

This stage has two jobs. One for deploying released artifacts into production and another for cleaning up Test environment

Deploying artifacts into production job is triggered manually

Cleaning up Test Environment un-deploys deployment, service and ingress resources used for TRDL service using kubectl commands

### Trigger Pipeline ###

Login to your Gitlab account and go to project "the-responding-dark-laughter"

Go to CI/CD -> Pipelines

Click on "Run Pipeline", check screenshot below for reference

[Screenshot 3](https://imgur.com/SePDH5w)


### Production Deployment description  ###

Remember Deploying TRDL service can only be done when all the stages in pipeline are complete successfully.

Deployment stage is triggered manually, so you need to follow ths steps below

1. Login to your Gitlab account and go to project "the-responding-dark-laughter"

2. Go to CI/CD -> Pipelines

3. Click on the last bubble of any completed pipeline, which gives a drop-down window and then click on play button for "Deploy-In-Prod-Env" job as shown in screenshot below

[Screenshot 4](https://imgur.com/gbO6Z0T)

4. After the job completes, look for a link in the logs to access TRDL service in production cluster
