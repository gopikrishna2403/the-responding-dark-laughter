import com.trdl.JettyServer;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestJettyServer {

    static JettyServer server = new JettyServer(2376);

    @BeforeClass
    public static void setup() throws Exception {
        server.start();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        server.stop();

    }


    @Test
    public void test_Response_Ok_111() throws IOException {
        String url = "http://localhost:2376/";
        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.connect();
        InputStream test = http.getInputStream();
        String result = new BufferedReader(new InputStreamReader(test)).lines().collect(Collectors.joining("\n"));
        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.OK_200));
        assertEquals("42", result);
    }


    @Test(expected = UnknownHostException.class)
    public void test_Invalid_Host_112() throws IOException {
        String url = "http://localho:2376/";
        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.connect();
    }

    @Test(expected = ConnectException.class)
    public void test_Invalid_Port_113() throws IOException {
        String url = "http://localhost:12345/";
        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.connect();
    }

    @Test
    public void test_Invalid_Resource_114() throws IOException {
        String url = "http://localhost:2376/asdasdasd";
        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.connect();
        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.NOT_FOUND_404));
    }


    @Test
    public void test_Invalid_Resource_With_Numbers_115() throws IOException {
        String url = "http://localhost:2376/123123123123123";
        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.connect();
        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.NOT_FOUND_404));
    }

    @Test
    public void test_Invalid_Resource_With_Special_charaters_116() throws IOException {
        String url = "http://localhost:2376/@@@@@@#############$$$$$$$$$$$$$$$$";
        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.connect();
        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.NOT_FOUND_404));
    }

    @Test
    public void test_Extra_Forward_Slashes_117() throws IOException {
        String url = "http://localhost:2376///////////////////////////";
        HttpURLConnection http = (HttpURLConnection) new URL(url).openConnection();
        http.connect();
        InputStream test = http.getInputStream();
        String result = new BufferedReader(new InputStreamReader(test)).lines().collect(Collectors.joining("\n"));
        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.OK_200));
        assertEquals("42", result);
    }


}
