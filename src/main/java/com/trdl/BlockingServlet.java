package com.trdl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BlockingServlet extends HttpServlet {
    protected void doGet(
            HttpServletRequest request,
            HttpServletResponse response)
            throws IOException {

//        Pattern pattern = Pattern.compile("([^\\/+])(\\w+|\\d+|[$&+,:;=?@#|'<>.-^*()%!])([^\\/+])");
        Pattern pattern = Pattern.compile("(\\w|\\d|[$&+,:;=?@#|'<>.-^*()%!])([^\\/+])");
        Matcher matcher = pattern.matcher(request.getRequestURI());

        if(!matcher.find()){
            response.setContentType("text/html");
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println("42");
        } else {
            response.setContentType("text/html");
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().println("Requested Resource is not implemented");
        }
    }
}
