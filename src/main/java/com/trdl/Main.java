package com.trdl;


import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.Properties;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    public static void main(String[] args) {
        JettyServer server = new JettyServer(Integer.parseInt(System.getenv("JETTY_SERVER_PORT")));
        //        PropertyConfigurator.configure("log4j.properties");
        Properties log4jProperties = new Properties();
        log4jProperties.setProperty("log4j.rootLogger", "INFO, console");
        log4jProperties.setProperty("log4j.appender.myConsoleAppender", "org.apache.log4j.ConsoleAppender");
        log4jProperties.setProperty("log4j.appender.console.Target", "System.out");
        log4jProperties.setProperty("log4j.appender.myConsoleAppender.layout", "org.apache.log4j.PatternLayout");
        log4jProperties.setProperty("log4j.appender.myConsoleAppender.layout.ConversionPattern", "%-5p %c %x - %m%n");
        PropertyConfigurator.configure(log4jProperties);
        logger.debug("Log4j appender configuration is successful !!");
        try {
            server.start();
            server.join();
        } catch (Exception e) {
//            System.out.println("Something bad happened");
            logger.error("Something bad happened", e);
        }
    }
}
