package com.trdl;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

import org.eclipse.jetty.servlet.ServletHolder;



public class JettyServer {

    private static final Logger logger = Logger.getLogger(JettyServer.class);

    private final Server server;

    public JettyServer(int port) {
        this.server = new Server(port);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        server.setHandler(context);
        context.addServlet(new ServletHolder(new BlockingServlet()), "/");
    }

    public void start() throws Exception {
        logger.debug("Starting Server");
        server.start();
        logger.debug("Started Server");
    }

    public void join() throws Exception {
        server.join();
    }

    public void stop() throws Exception {
        server.stop();
    }


}

