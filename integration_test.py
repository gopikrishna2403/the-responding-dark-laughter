import requests
import os

# Get environment variables
URL = os.getenv('APP_URL')

HOSTNAME = f'http://{URL}'

test1=requests.get(HOSTNAME)
if test1.text.strip() == "42":
    print(test1.text.strip())
    print("Test1 passed")
else:
    print(test1.text.strip())
    print("Test1 failed")
    exit(1)
    
test2=requests.get(HOSTNAME + '///////')
if test2.text.strip() == "42":
    print(test2.text.strip())
    print("Test2 passed")
else:
    print(test2.text.strip())
    print("Test2 failed")
    exit(1)


test3=requests.get(HOSTNAME + '/asdasdasd/asdasdas/111/@@')
if test3.text.strip() == "Requested Resource is not implemented":
    print(test2.text.strip())
    print("Test3 passed")
else:
    print(test2.text.strip())
    print("Test3 failed")
    exit(1)